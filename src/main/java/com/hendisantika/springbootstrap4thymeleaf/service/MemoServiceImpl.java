package com.hendisantika.springbootstrap4thymeleaf.service;

import com.hendisantika.springbootstrap4thymeleaf.entity.Memo;
import com.hendisantika.springbootstrap4thymeleaf.repository.MemoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-bootstrap4-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/10/18
 * Time: 08.02
 * To change this template use File | Settings | File Templates.
 */
@Service
public class MemoServiceImpl implements MemoService {

    final private MemoRepository memoRepository;

    public MemoServiceImpl(MemoRepository memoRepository) {
        this.memoRepository = memoRepository;
    }

    @Override
    public Page<Memo> findAll(Pageable page) {
        return memoRepository.findAll(page);
    }

}