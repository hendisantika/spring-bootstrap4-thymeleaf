package com.hendisantika.springbootstrap4thymeleaf.service;

import com.hendisantika.springbootstrap4thymeleaf.entity.Memo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-bootstrap4-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/10/18
 * Time: 08.00
 * To change this template use File | Settings | File Templates.
 */
public interface MemoService {
    Page<Memo> findAll(Pageable page);
}