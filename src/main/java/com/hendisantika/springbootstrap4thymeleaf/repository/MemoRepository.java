package com.hendisantika.springbootstrap4thymeleaf.repository;

import com.hendisantika.springbootstrap4thymeleaf.entity.Memo;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-bootstrap4-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 02/10/18
 * Time: 07.58
 * To change this template use File | Settings | File Templates.
 */
public interface MemoRepository extends JpaRepository<Memo, Long> {
}