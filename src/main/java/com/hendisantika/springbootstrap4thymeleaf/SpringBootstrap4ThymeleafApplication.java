package com.hendisantika.springbootstrap4thymeleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootstrap4ThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootstrap4ThymeleafApplication.class, args);
    }
}
