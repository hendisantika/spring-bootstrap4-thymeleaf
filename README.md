# Spring Bootstrap 4 Thymeleaf Demo

Run this project by this command :

`mvn clean spring-boot:run`

### Screenshot

Demo Page

![Demo Page](img/demo.png "Demo Page")

Memo List Page

![Memo List Page](img/memo.png "Memo List Page")

Input Page

![Input Page](img/input.png "Input Page")

Validation Page

![Validation Page](img/validation.png "Validation Page")

Confirmation Page

![Confirmation Page](img/confirm.png "Confirmation Page")