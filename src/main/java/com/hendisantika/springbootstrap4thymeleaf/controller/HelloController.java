package com.hendisantika.springbootstrap4thymeleaf.controller;

import com.hendisantika.springbootstrap4thymeleaf.model.ItemDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-bootstrap4-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/10/18
 * Time: 07.00
 * To change this template use File | Settings | File Templates.
 */
@Controller
@Slf4j
public class HelloController {

    @GetMapping(path = {"", "hello"})
    public String greeting(Model model) {
        model.addAttribute("message", "HELLO WORLD");
        model.addAttribute("date", LocalDateTime.of(2018, 4, 13, 11, 12, 13));
        List<ItemDto> items = Arrays.asList(
                ItemDto.of(1L, "Umbrella", 10, LocalDateTime.now()),
                ItemDto.of(2L, "Fried potato", 30, LocalDateTime.now()),
                ItemDto.of(3L, "Fried Chicken", 10, LocalDateTime.now()),
                ItemDto.of(4L, "Teh manis", 40, LocalDateTime.now())
        );
        model.addAttribute("items", items);
        return "hello";
    }

    @GetMapping(path = "checkout")
    public String checkout() {
        return "checkout";
    }
}