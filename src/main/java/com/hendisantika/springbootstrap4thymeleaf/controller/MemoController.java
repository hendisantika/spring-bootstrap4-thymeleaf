package com.hendisantika.springbootstrap4thymeleaf.controller;

import com.hendisantika.springbootstrap4thymeleaf.entity.Memo;
import com.hendisantika.springbootstrap4thymeleaf.service.MemoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-bootstrap4-thymeleaf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/10/18
 * Time: 07.04
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("memo")
public class MemoController {

    final private MemoService memoService;

    public MemoController(MemoService memoService) {
        this.memoService = memoService;
    }

    @GetMapping
    public String list(Pageable page, Model model) {
        Page<Memo> memos = memoService.findAll(page);
        model.addAttribute("memos", memos);
        return "memo";
    }

}